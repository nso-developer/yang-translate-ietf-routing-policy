from typing import Optional
import ipaddress

import ncs
from ncs.application import Service
from .utils import DeviceOs, get_dev_os
from . import TemplateName


def generate_ios_xr_rpl_prefix_set(prefix_set: ncs.maagic.ListElement) -> str:
    statements = []
    for p in prefix_set.prefixes.prefix_list:
        ipn = ipaddress.ip_network(p.ip_prefix)
        if prefix_set.mode == 'ipv4' and ipn.prefixlen == 32 or prefix_set.mode == 'ipv6' and ipn.prefixlen == 128:
            statements.append(str(ipn.network_address))
        elif ipn.prefixlen == p.mask_length_lower and ipn.prefixlen == p.mask_length_upper:
            statements.append(str(p.ip_prefix))
        elif ipn.prefixlen == p.mask_length_lower:
            statements.append(f'{p.ip_prefix} le {p.mask_length_upper}')
        elif prefix_set.mode == 'ipv4' and p.mask_length_upper == 32 or prefix_set.mode == 'ipv6' and p.mask_length_upper == 128:
            statements.append(f'{p.ip_prefix} ge {p.mask_length_lower}')
        elif p.mask_length_lower == p.mask_length_upper:
            statements.append(f'{p.ip_prefix} eq {p.mask_length_lower}')
        else:
            statements.append(f'{p.ip_prefix} ge {p.mask_length_lower} le {p.mask_length_upper}')
    rpl = f'prefix-set {prefix_set.name}\n' + ',\n'.join(f'  {statement}' for statement in statements) + '\nend-set\n'
    return rpl


def generate_ios_xr_rpl_tag_set(tag_set: ncs.maagic.ListElement) -> str:
    rpl = f'tag-set {tag_set.name}\n'  + ',\n'.join(f'  {tag_value}' for tag_value in tag_set.tag_value) + '\nend-set\n'
    return rpl


def generate_ios_xr_rpl_policy_definition(policy: ncs.maagic.ListElement, hierarchical: Optional[bool] = False) -> str:
    rpl = f'route-policy {policy.name}{"-HIERARCHICAL" if hierarchical else ""}\n'
    for stmt in policy.statements.statement:

        # Format the conditions
        conditions = []

        if stmt.conditions.call_policy:
            conditions.append(f'(apply {stmt.conditions.call_policy}-HIERARCHICAL)')

        if stmt.conditions.source_protocol:
            if stmt.conditions.source_protocol == 'rt:direct':
                conditions.append('(protocol is connected)')
            # We are silently ignoring static & bgp as a protocols as XR does not accept it in our
            # attachment points and we don't seem to need it elsewhere ...

        if stmt.conditions.match_prefix_set.prefix_set:
            mps = stmt.conditions.match_prefix_set
            if mps.match_set_options == 'all':
                raise ValueError('Matching for all prefixes in a prefix-set is not supported on IOS-XR')
            conditions.append(f'({"not " if mps.match_set_options == "invert" else ""}destination in {mps.prefix_set})')

        if stmt.conditions.match_tag_set.tag_set:
            mts = stmt.conditions.match_tag_set
            if mts.match_set_options == 'all':
                raise ValueError('Matching for all tags in a tag-set is not supported on IOS-XR')
            conditions.append(f'({"not " if mts.match_set_options == "invert" else ""}tag in {mts.tag_set})')

        if stmt.conditions.bgp_conditions.match_community_set.community_set:
            mcs = stmt.conditions.bgp_conditions.match_community_set
            if mcs.match_set_options == 'any':
                conditions.append(f'(community matches-any {mcs.community_set})')
            elif mcs.match_set_options == 'all':
                conditions.append(f'(community matches-every {mcs.community_set})')
            else:
                conditions.append(f'(not community matches-any {mcs.community_set})')

        if stmt.conditions.bgp_conditions.match_as_path_set.as_path_set:
            maps = stmt.conditions.bgp_conditions.match_as_path_set
            if maps.match_set_options == 'all':
                raise ValueError('Matching for all as-paths in an as-path-set is not supported on IOS-XR')
            conditions.append(f'({"not " if maps.match_set_options == "invert" else ""}as-path in {maps.as_path_set})')

        # Merge conditions together
        if conditions:
            rpl += '  if '
            rpl += ' and '.join(conditions)
            rpl += ' then\n'

        # Add in actions
        if stmt.actions.bgp_actions.set_route_origin:
            rpl += f'{"  " if conditions else ""}  set origin {stmt.actions.bgp_actions.set_route_origin}\n'
        if stmt.actions.bgp_actions.set_local_pref:
            rpl += f'{"  " if conditions else ""}  set local-preference {stmt.actions.bgp_actions.set_local_pref}\n'
        if stmt.actions.bgp_actions.set_next_hop:
            rpl += f'{"  " if conditions else ""}  set next-hop {stmt.actions.bgp_actions.set_next_hop}\n'
        if stmt.actions.bgp_actions.set_med:
            med = 'igp-cost' if stmt.actions.bgp_actions.set_med == 'igp' else stmt.actions.bgp_actions.set_med
            rpl += f'{"  " if conditions else ""}  set med {med}\n'
        if stmt.actions.bgp_actions.set_community.community_set_ref:
            if stmt.actions.bgp_actions.set_community.options == 'add':
                rpl += f'{"  " if conditions else ""}  set community {stmt.actions.bgp_actions.set_community.community_set_ref} additive\n'
            if stmt.actions.bgp_actions.set_community.options == 'replace':
                rpl += f'{"  " if conditions else ""}  set community {stmt.actions.bgp_actions.set_community.community_set_ref}\n'
            elif stmt.actions.bgp_actions.set_community.options == 'remove':
                rpl += f'{"  " if conditions else ""}  delete community in {stmt.actions.bgp_actions.set_community.community_set_ref}\n'
        elif stmt.actions.bgp_actions.set_community.communities:
            communities = ", ".join(str(c) for c in stmt.actions.bgp_actions.set_community.communities)
            if stmt.actions.bgp_actions.set_community.options == 'add':
                rpl += f'{"  " if conditions else ""}  set community ({communities}) additive\n'
            elif stmt.actions.bgp_actions.set_community.options == 'replace':
                rpl += f'{"  " if conditions else ""}  set community ({communities})\n'
            elif stmt.actions.bgp_actions.set_community.options == 'remove':
                rpl += f'{"  " if conditions else ""}  delete community in ({communities})\n'
        if not hierarchical:
            if stmt.actions.policy_result == "accept-route":
                rpl += f'{"  " if conditions else ""}  done\n'
            elif stmt.actions.policy_result == "reject-route":
                rpl += f'{"  " if conditions else ""}  drop\n'
        else:
            # call policies are supported on XR through "if apply ..." but we require a syntax variation when doing hierarchical
            # https://www.cisco.com/c/en/us/td/docs/routers/asr9000/software/asr9k-r6-5/routing/configuration/guide/b-routing-cg-asr9000-65x/b-routing-cg-asr9000-65x_chapter_01011.html#reference_22A718AA9148498394E24A71027DE0BA
            if stmt.actions.policy_result == "accept-route":
                rpl += f'{"  " if conditions else ""}  pass\n'
                rpl += f'{"  " if conditions else ""}  done\n'
            elif stmt.actions.policy_result == "reject-route":
                rpl += f'{"  " if conditions else ""}  done\n'

        if conditions:
            rpl += '  endif\n'
    rpl += 'end-policy\n'
    return rpl


def generate_ios_xr_rpl_community_set(community_set: ncs.maagic.ListElement) -> str:
    statements = []
    for m in community_set.member:
        if all(char.isdigit() or char == ':' for char in str(m)):
            statements.append(m)
        else:
            statements.append(f'ios-regex \'{m}\'')
    rpl = f'community-set {community_set.name}\n' + ',\n'.join(f'  {statement}' for statement in statements) + '\nend-set\n'
    return rpl


def generate_ios_xr_rpl_as_path_set(as_path_set: ncs.maagic.ListElement) -> str:
    rpl = f'as-path-set {as_path_set.name}\n' + ',\n'.join(f'  ios-regex \'{member}\'' for member in as_path_set.member) + '\nend-set\n'
    return rpl


class PrefixSetTranslateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info(f"{service._path}: service create()")

        errors = []
        for p in service.prefixes.prefix_list:
            ip_prefix = ipaddress.ip_network(p.ip_prefix)
            # The device MUST validate that all prefixes are of the selected type
            if service.mode == 'ipv4' and not isinstance(ip_prefix, ipaddress.IPv4Network):
                errors.append(f'{p.ip_prefix} is not a valid for prefix-set mode ipv4')
            elif service.mode == 'ipv6' and not isinstance(ip_prefix, ipaddress.IPv6Network):
                errors.append(f'{p.ip_prefix} is not a valid for prefix-set mode ipv6')
            # Mask length range lower bound MUST NOT be less than the prefix length defined in ip-prefix.
            if p.mask_length_lower < ip_prefix.prefixlen:
                errors.append(f'Mask length lower bound ({p.mask_length_lower}) is less than prefix length on {p.ip_prefix}')
        if errors:
            raise ValueError('\n'.join(errors))

        template = ncs.template.Template(service)
        tvars = ncs.template.Variables()
        tvars.add('IOS_XR_RPL', generate_ios_xr_rpl_prefix_set(service))
        template.apply(TemplateName('prefix-set'), tvars)


class TagSetTranslateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info(f"{service._path}: service create()")
        template = ncs.template.Template(service)
        tvars = ncs.template.Variables()
        tvars.add('IOS_XR_RPL', generate_ios_xr_rpl_tag_set(service))
        template.apply(TemplateName('tag-set'), tvars)


class PolicyDefinitionTranslateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info(f"{service._path}: service create()")
        device = service._parent._parent._parent._parent._parent
        os = get_dev_os(device)
        template = ncs.template.Template(service)

        if os == DeviceOs.IOSXR:
            for stmt in service.statements.statement:
                if stmt.conditions.match_prefix_set.prefix_set and stmt.conditions.match_prefix_set.match_set_options == 'all':
                    raise ValueError('Matching for all prefixes in a prefix-set is not supported on IOS-XR')
                if stmt.conditions.match_tag_set.tag_set and stmt.conditions.match_tag_set.match_set_options == 'all':
                    raise ValueError('Matching for all tags in a tag-set is not supported on IOS-XR')
                if stmt.conditions.bgp_conditions.match_as_path_set.as_path_set and stmt.conditions.bgp_conditions.match_as_path_set.match_set_options == 'all':
                    raise ValueError('Matching for all as-paths in an as-path-set is not supported on IOS-XR')
        elif os == DeviceOs.SROS_NC:
            for stmt in service.statements.statement:
                if stmt.conditions.match_tag_set.tag_set:
                    tag_set = service._parent._parent._parent.defined_sets.tag_sets.tag_set[stmt.conditions.match_tag_set.tag_set]
                    if len(tag_set.tag_value) > 1:
                        raise ValueError('A maximum of one tag per tag-set is supported on SROS')
                    if stmt.conditions.match_tag_set.match_set_options in ('all', 'invert'):
                        raise ValueError('Matching for all/inverted tags in a tag-set is not supported on SROS')
                if stmt.conditions.match_prefix_set.prefix_set:
                    if stmt.conditions.match_prefix_set.match_set_options == 'invert':
                        tvars = ncs.template.Variables()
                        tvars.add('PREFIX_SET', stmt.conditions.match_prefix_set.prefix_set)
                        template.apply(TemplateName('policy-definition-pefix-set-inverted'), tvars)
                    elif stmt.conditions.match_prefix_set.match_set_options == 'all':
                        raise ValueError('Matching for all prefixes in a prefix-set is not supported on SROS')
                if stmt.conditions.bgp_conditions.match_as_path_set.as_path_set:
                    if stmt.conditions.bgp_conditions.match_as_path_set.match_set_options == 'invert':
                        tvars = ncs.template.Variables()
                        tvars.add('AS_PATH_SET', stmt.conditions.bgp_conditions.match_as_path_set.as_path_set)
                        template.apply(TemplateName('policy-definition-as-path-set-inverted'), tvars)
                    elif stmt.conditions.bgp_conditions.match_as_path_set.match_set_options == 'all':
                        raise ValueError('Matching for all as-paths in an as-path-set is not supported on SROS')
                if stmt.actions.bgp_actions.set_community.communities:
                    raise ValueError('Defining communities inline is not supported on SROS')
        elif os == DeviceOs.JUNOS:
            for stmt in service.statements.statement:
                if stmt.conditions.match_tag_set.tag_set:
                    tag_set = service._parent._parent._parent.defined_sets.tag_sets.tag_set[stmt.conditions.match_tag_set.tag_set]
                    if len(tag_set.tag_value) > 1:
                        raise ValueError('A maximum of one tag per tag-set is supported on JUNOS')
                    if stmt.conditions.match_tag_set.match_set_options in ('all', 'invert'):
                        raise ValueError('Matching for all/inverted tags in a tag-set is not supported on JUNOS')
                if stmt.conditions.match_prefix_set.prefix_set:
                    if stmt.conditions.match_prefix_set.match_set_options == 'invert':
                        tvars = ncs.template.Variables()
                        tvars.add('PREFIX_SET', stmt.conditions.match_prefix_set.prefix_set)
                        template.apply(TemplateName('policy-definition-pefix-set-inverted'), tvars)
                    elif stmt.conditions.match_prefix_set.match_set_options == 'all':
                        raise ValueError(f'Matching for all prefixes in a prefix-set is not supported on JUNOS {service.name}')
                if stmt.conditions.bgp_conditions.match_as_path_set.as_path_set:
                    if stmt.conditions.bgp_conditions.match_as_path_set.match_set_options == 'invert':
                        tvars = ncs.template.Variables()
                        tvars.add('AS_PATH_SET', stmt.conditions.bgp_conditions.match_as_path_set.as_path_set)
                        template.apply(TemplateName('policy-definition-as-path-set-inverted'), tvars)
                    elif stmt.conditions.bgp_conditions.match_as_path_set.match_set_options == 'all':
                        raise ValueError('Matching for all as-paths in an as-path-set is not supported on JUNOS')
                if stmt.conditions.bgp_conditions.match_community_set.community_set:
                    if stmt.conditions.bgp_conditions.match_community_set.match_set_options == 'any':
                        community_set = service._parent._parent._parent.defined_sets.bgp_defined_sets.community_sets.community_set[stmt.conditions.bgp_conditions.match_community_set.community_set]
                        if len(community_set.member) > 1:
                            raise ValueError(f'A maximum of one community in a community-set is supported on SROS when matching for any communities {service.name}')
                if stmt.actions.bgp_actions.set_community.communities:
                    raise ValueError('Defining communities inline is not supported on JUNOS')

        tvars = ncs.template.Variables()
        if service.ios_xr_policy_type in ('regular', 'both'):
            tvars.add('IOS_XR_NAME', service.name)
            tvars.add('IOS_XR_RPL', generate_ios_xr_rpl_policy_definition(service))
            template.apply(TemplateName('policy-definition'), tvars)
        if service.ios_xr_policy_type in ('hierarchical', 'both'):
            tvars.add('IOS_XR_NAME', f'{service.name}-HIERARCHICAL')
            tvars.add('IOS_XR_RPL', generate_ios_xr_rpl_policy_definition(service, True))
            template.apply(TemplateName('policy-definition'), tvars)

class CommunitySetTranslateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info(f"{service._path}: service create()")

        template = ncs.template.Template(service)
        tvars = ncs.template.Variables()
        tvars.add('IOS_XR_RPL', generate_ios_xr_rpl_community_set(service))
        template.apply(TemplateName('community-set'), tvars)


class ASPathSetTranslateService(ncs.application.Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info(f"{service._path}: service create()")
        template = ncs.template.Template(service)
        tvars = ncs.template.Variables()
        tvars.add('IOS_XR_RPL', generate_ios_xr_rpl_as_path_set(service))
        template.apply(TemplateName('as-path-set'), tvars)


class IETF_Routing_Policy(ncs.application.Application):
    def setup(self):
        self.log.info('IETF_Routing_Policy RUNNING')
        self.register_service('rp-translate-prefix-set-servicepoint', PrefixSetTranslateService)
        self.register_service('rp-translate-tag-set-servicepoint', TagSetTranslateService)
        self.register_service('rp-translate-policy-definition-servicepoint', PolicyDefinitionTranslateService)
        self.register_service('rp-translate-community-set-servicepoint', CommunitySetTranslateService)
        self.register_service('rp-translate-as-path-set-servicepoint', ASPathSetTranslateService)

    def teardown(self):
        self.log.info('IETF_Routing_Policy FINISHED')
