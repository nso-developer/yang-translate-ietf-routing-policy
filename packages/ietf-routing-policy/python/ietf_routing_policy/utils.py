import enum


class DeviceOs(enum.Enum):
    SROS_NC = "SROS_NC"
    IOSXR = "IOSXR"
    JUNOS = "JUNOS"
    UNKNOWN = "UNKNOWN"


def get_dev_os(dev):
    os = None
    for cap in dev.capability:
        if cap.uri == "urn:nokia.com:sros:ns:yang:sr:conf":
            os = DeviceOs.SROS_NC
        if cap.uri == "http://cisco.com/ns/yang/cisco-xr-types":
            os = DeviceOs.IOSXR
        if cap.uri == "urn:juniper-rpc":
            os = DeviceOs.JUNOS

    if os is None:
        os = DeviceOs.UNKNOWN

    return os
